package com.scripps.qa;

import org.graphwalker.core.condition.EdgeCoverage;
import org.graphwalker.core.generator.RandomPath;
import org.graphwalker.java.test.TestBuilder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

/**
 * Created by 161803 on 8/10/2015.
 */
public class gmailTest_Test {

    private final static Path MODEL_A = Paths.get("com/scripps/qa/gmailTest.graphml");

    private static WebDriver driver;// = new FirefoxDriver();
    public final static String BASE_URL = "https://gmail.com";
    private final static Logger logger = LoggerFactory.getLogger(gmailTest_Test.class);
    DesiredCapabilities caps;
    //  https://gmail.com/intl/en/mail/help/about.html

    @Parameters("browser")
    @BeforeClass
    public void launchApplication(String browser) throws MalformedURLException {
        if(browser.equalsIgnoreCase("firefox")) {
          //  driver = new FirefoxDriver();
            caps = DesiredCapabilities.firefox();
            driver = new RemoteWebDriver(new URL("http://SNKXW-SMANIKON2:4445/wd/hub"), caps);
            driver.get(BASE_URL + "/intl/en/mail/help/about.html"); // Navigate to login
            logger.info("BeforeClass firefox -The application starting now.");
        }else if (browser.equalsIgnoreCase("ie")) {
            // Here I am setting up the path for my IEDriver
            //   File file = new File("C://selenium_2440//IEDriverServer.exe");
            // File file = new File("IEDriverServer.exe");

            System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
             caps = DesiredCapabilities.internetExplorer();
            caps = DesiredCapabilities.internetExplorer();
            caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
           // driver = new InternetExplorerDriver(caps);
            driver = new RemoteWebDriver(new URL("http://SNKXW-SMANIKON2:4445/wd/hub"), caps);
            driver.get(BASE_URL + "/intl/en/mail/help/about.html"); // Navigate to login
            logger.info("BeforeClass-ie - The application starting now.");
        }
        else if (browser.equalsIgnoreCase("chrome")) {
            // Here I am setting up the path for my IEDriver
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
           // driver = new ChromeDriver();
            caps = DesiredCapabilities.chrome();
            driver = new RemoteWebDriver(new URL("http://SNKXW-SMANIKON2:4445/wd/hub"), caps);
            driver.get(BASE_URL + "/intl/en/mail/help/about.html"); // Navigate to login
            logger.info("BeforeClass-ie - The application starting now.");
        }



     //   driver = new FirefoxDriver();
      //  driver.get(BASE_URL + "/intl/en/mail/help/about.html"); // Navigate to login
      //  logger.info("BeforeClass-The application starting now.");


    }

    @AfterClass
    public void closeApplication() throws InterruptedException {
      //  Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.close(); // Close the browser
       // driver.quit();
      //  Thread.sleep(2000);
        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        if (driver != null) {
            driver.quit();
        }
        logger.info("AfterClass-The application was closed now.");
    }

    @Test
    public void runFunctionalTest() throws Exception {

        //logger.info("runFunctionalTest: Selenium is starting ");
        driver.get(BASE_URL + "/intl/en/mail/help/about.html");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("BeforeClass-The application starting now.");

        /////////////////
        gmailTest_impl GTest = new gmailTest_impl(driver);
        new TestBuilder()
                .addModel(MODEL_A, GTest.setPathGenerator(new RandomPath(new EdgeCoverage(100))))
                .execute();
       // driver.close(); // Close the browser
      //  driver.quit();
    }

}
