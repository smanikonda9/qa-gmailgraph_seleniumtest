package com.scripps.qa;

import org.graphwalker.core.machine.ExecutionContext;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by 161803 on 8/10/2015.
 */
public class gmailTest_impl extends ExecutionContext implements gmailTest{
    String username = "qascripps@gmail.com";
    String password = "Sai123456";
    private static WebDriver driver;
    private final static Logger logger = LoggerFactory.getLogger(gmailTest_impl.class);
    public gmailTest_impl(WebDriver driver) {
        this.driver=driver;
    }

    @Override
    public void aClickOnSignOut_Link() {
        logger.info("aClickOnSignOut_Link ");
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
          driver.manage().timeouts().implicitlyWait(30, TimeUnit.MINUTES);
 //       driver.findElement(By.xpath("//span[@class='gb_k']")).click();
        driver.findElement(By.xpath("//a[@href='https://accounts.google.com/SignOutOptions?hl=en&continue=https://mail.google.com/mail&service=mail']")).click();
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.MINUTES);
        driver.switchTo().activeElement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.MINUTES);
        driver.findElement(By.xpath("//a[contains(.,'Sign out')]")).click();
     //   driver.findElement(By.xpath("//a[@class='gb_hc gb_oc gb_a']")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.MINUTES);
        // driver.switchTo().defaultContent();
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void aClickOn_SigninLink() {

        String logouttext="gmail-sign-in";
        String tempLinktext="";
        tempLinktext= driver.findElement(By.xpath("//a[@id='gmail-sign-in']")).getText();

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.MINUTES);
        driver.findElement(By.xpath("//a[@id='gmail-sign-in']")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        if(tempLinktext.contains(logouttext)){
            logger.info("PASS: aClickOn_SigninLink -In the application the gmail-sign-in link correctly displays \"{}\" link.", tempLinktext);

        } else {
            logger.info("FAIL: aClickOn_SigninLink  -In the application the gmail-sign-in link correctly  displayed \"{}\" instead of \"{}\".", tempLinktext,logouttext);
        }

//        logger.info("aClickOn_SigninLink ");
//        driver.findElement(By.xpath("//a[@id='gmail-sign-in']")).click();

    }

    @Override
    public void aClickOn_SigninButton() {
        logger.info("aClickOn_SigninButton ");
        //driver.findElement(By.xpath("//a[@id='gmail-sign-in']")).click();
        driver.findElement(By.xpath("//input[@id='Email']")).clear();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(username);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@id='next']")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@id='Passwd']")).clear();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@id='Passwd']")).sendKeys(password);

        driver.findElement(By.xpath("//input[@id='signIn']")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        String requiredLoginTitle ="Inbox";
        String actualTitle = driver.getTitle();
        logger.debug(actualTitle);
        if (actualTitle.contains(requiredLoginTitle)) {
            logger.info("PASS: aClickOn_SigninButton- The username \"{}\"  and password \"{}\"are entered in the \"{}\" .", new String[]{username, password, actualTitle});
            //   logger.info("PASS: aClickOn_SigninButton- The application Inbox screen correctly displays \"{}\".", requiredLoginTitle);
        }else {
            logger.info("FAIL: aClickOn_SigninButton- The gmail login screen was not displayed.");
            //logger.info("FAIL: aClickOn_SigninButton- The application Inbox screen displayed \"{}\" instead of \"{}\".",actualTitle, requiredLoginTitle);
        }
        logger.debug("end of aClickOn_SigninButton ");

    }

    @Override
    public void vGmail_screen() {
        String requiredLoginTitle ="Gmail - Free Storage and Email from Google";
        String actualTitle = driver.getTitle();
        logger.debug(actualTitle);
        if (actualTitle.contains(requiredLoginTitle)) {
            logger.info("PASS: vGmail_screen- The application gmail screen correctly displays \"{}\".", requiredLoginTitle);}
        else {
            logger.info("FAIL: vGmail_screen- The application gmail screen displayed \"{}\" instead of \"{}\".",actualTitle, requiredLoginTitle);
        }
        logger.debug("end of vGmail_screen ");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

//        logger.info("vGmail_screen ");
//        driver.manage().window().maximize();
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        String title = "Gmail - Free Storage and Email from Google";
//        if (driver.getTitle().contains(title)){
//            System.out.println("Logged in sucessfully !!!"+driver.getTitle());
//        }
//        else {
//            System.out.println("Unable to loggin :-( "+driver.getTitle());
//        }
    }

    @Override
    public void vInbox_Screen() {

        String requiredLoginTitle ="Inbox";
        String actualTitle = driver.getTitle();
        logger.debug(actualTitle);
        if (actualTitle.contains(requiredLoginTitle)) {
            logger.info("PASS: vInbox_Screen- The application Inbox screen correctly displays \"{}\".", requiredLoginTitle);}
        else {
            logger.info("FAIL: vInbox_Screen- The application Inbox screen displayed \"{}\" instead of \"{}\".",actualTitle, requiredLoginTitle);
        }
        logger.debug("end of vInbox_Screen ");



//        logger.info("vInbox_Screen ");
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        String InboxTitle = "Inbox";
//        if (driver.getTitle().contains(InboxTitle)){
//            System.out.println("Logged in sucessfully !!!"+driver.getTitle());
//        }
//        else {
//            System.out.println("Unable to loggin :-( "+driver.getTitle());
//        }
    }

    @Override
    public void aINIT() {
        //logger.info("aINIT ");
        logger.info("Info: aINIT- The application login screen");
    }

    @Override
    public void vGmailLogin_screen() {

        String requiredLoginTitle ="Gmail";
        String actualTitle = driver.getTitle();
        logger.debug(actualTitle);
        if (actualTitle.contains(requiredLoginTitle)) {
            logger.info("PASS: vGmailLogin_screen- The application login screen correctly displays \"{}\".", requiredLoginTitle);}
        else {
            logger.info("FAIL: vGmailLogin_screen- The application login screen displayed \"{}\" instead of \"{}\".",actualTitle, requiredLoginTitle);
        }
        logger.debug("end of vGmailLogin_screen ");

//
//        logger.info("vGmailLogin_screen ");
//        String title = "Gmail";
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        if (driver.getTitle().contains(title)){
//            System.out.println("Logged in sucessfully !!!"+driver.getTitle());
//        }
//        else {
//            System.out.println("Unable to loggin :-( "+driver.getTitle());
//        }
    }
}
